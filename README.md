# README #

This is a skeleton project for personal use, to quickly create Java Maven projects with 

* Guava
* Logback & Slf4j
* Lombok
* Junit4
* shell script to run main